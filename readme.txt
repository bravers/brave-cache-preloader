=== Preload Fullpage Cache ===
Contributors: Brave Tecnologia, Marcio Amicci, Anderson Lopes
Donate link: https://bravegroup.com.br
Tags: preload, cache, performance, pre-caching
Requires at least: 3.0
Tested up to: 5.4
Stable tag: 1.0
License: GPLv3
Requires PHP: 5.3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Preloads any new ou updated posts with this cache plugin. This requires a CDN or a fullpage cache plugin to maximize your performance, such a CloudFront, Cloudflare, goCache, Varnish or cache plugins.

== Description ==

This plugin was developed to solve high traffic problems, where several people access the database before the first caching occurs.

= What this plugin does: =

* Before the effective publication of the article, this plugin uses the HTTP API of wordpress to perform an early caching of the page before users access its content and cause a high level of CPU consumption in the database.
* This plugin works on updating and inserting posts.
* Works for any type of device, the cache is also generated in responsive versions.
* Works for any post type.

= What this plugin does not do:

* This plugin is not a cache plugin, it only makes a request for the plugins to pre-cache the content.

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/brave-cache-preloader` folder or install the plugin through the WordPress plugins screen.
1. Activate the plugin in the 'Plugins' screen in WordPress
1. It's ok for use.

== Frequently Asked Questions ==

= Where is the settings ? =

This plugin dont have settings, but we can provide if necessary (in future versions).

= How do I test the funcionality? =

You can check it out by looking at your database CPU performance graphs after publication. :). You can also look at the graphics of your CND or the content of your cache layer plugins.

= Why does this plugin slow down publishing and updating posts? =


The plugin must access the page in question without using a cache, so this can cause some slowness when publishing or updating the post.

== Screenshots ==

* assets/screenshot.png

== Upgrade Notice ==
none

== Changelog ==

= 1.0.0 =
* First commit
