# README #

## BRAVE CACHE PRELOADER PLUGIN ##
Preloads any new ou updated posts with this cache plugin. This requires a CDN or a fullpage cache plugin to maximize your performance, such a CloudFront, Cloudflare, goCache, Varnish or cache plugins.

### About this plugin ###

* This plugin was developed to solve high traffic problems, where several people access the database before the first caching occurs.
* V. 1.0
* https://bitbucket.org/bravers/brave-cache-preloader/src/master/

### What this plugin does ###

* Before the effective publication of the article, this plugin uses the HTTP API of wordpress to perform an early caching of the page before users access its content and cause a high level of CPU consumption in the database.
* This plugin works on updating and inserting posts.
* Works for any type of device, the cache is also generated in responsive versions.
* Works for any post type.

### What this plugin does not do: ###

* This plugin is not a cache plugin, it only makes a request for the plugins to pre-cache the content.

### Contribution guidelines ###

* Writing tests: Winov Tecnologia, BraveGroup, R3A Tecnologia, goCache Plugin
* Code review: Marcio Amicci, Anderson Lopes

### Who do I talk to? ###

* marcio.amicci@bravegroup.com.br
* anderson.lopes@bravegroup.com.br
* marcello@bravegroup.com.br