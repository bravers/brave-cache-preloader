<?php
/*
Plugin Name: Brave Cache Preloader
Version: 1.0
Plugin URI: https://bitbucket.org/bravers/brave-cache-preloader/src/master/
Author: Bravegroup - Marcio Amicci
Author URI: https://bravegroup.com.br
Description: Preloads any new ou updated posts with this cache plugin.
*/

// disable executing this script directly
defined( 'ABSPATH'  ) or die( 'No funny here please!' );

if( !class_exists('BRAVE_CACHE_PRELOADER') ) {
    class BRAVE_CACHE_PRELOADER
    {
        function __construct() {
            add_action( 'wp_insert_post', array( $this, 'brave_pre_desktop' ),  900, 3 ); // for desktop version
            add_action( 'wp_insert_post', array( $this, 'brave_pre_mobile' ),   990, 3 ); // for mobile version
            add_action( 'wp_insert_post', array( $this, 'brave_pre_amp' ), 999, 3 ); // for amp version
        }

        // desktop version using Chrome and AppleKit
        function brave_pre_desktop( $post_ID, $post, $update ) {
            $desktop_url = get_permalink( $post_ID );
            $desktop_url_args = array(
                'httpversion' => '1.1',
                'user-agent'  => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36',
            );
            wp_remote_get( $desktop_url, $desktop_url_args );
        }

        // mobile version using Apple IOS
        function brave_pre_mobile( $post_ID, $post, $update ) {
            $mobile_url = get_permalink( $post_ID );
            $mobile_url_args = array(
                'httpversion' => '1.1',
                'user-agent'  => 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
            ); 
            wp_remote_get( $mobile_url, $mobile_url_args );
        }

        // AMP version using Chrome and Sierra
        function preload_amp( $post_ID, $post, $update ) {
            $amp_url = get_permalink( $post_ID ) . 'amp/';
            $amp_url_args = array(
                'httpversion' => '1.1',
                'user-agent'  => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36',
            );
            wp_remote_get( $amp_url, $amp_url_args );
        }
    } // BRAVE_CACHE_PRELOADER end :)

    // initialize the plugin by creating a new object
    $GLOBALS['brave-cache-preloader'] = new BRAVE_CACHE_PRELOADER();

} // if class_exists BRAVE_CACHE_PRELOADER
